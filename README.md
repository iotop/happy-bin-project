# Happy Bin Project

Repository for all things "Bin Project"

Requirements:  
- Please ensure you copy DFplayer library to your Arduino library folder to use DF player functionalities.
- Volume level and distance can be adjusted from the code. (Design school may ask for us to do that during fitting).
- A mini SD card will need to be reformatted in FAT format.  This project only will play the first mp3 file in the SD.  Name convention is 0001.mp3.  

/*
 * Project : Happy Bin
 * Author : Bee Kaan
 * Date : 09/08/19
 * Description : This project uses Ultrasonic sensor to monitor any movement.  Any close movement will trigger, random colors of RGB light and mp3 player to play mp3 file (s).
 * Requirement : mp3 files in SD card will need to be renamed into 0001.mp3 format.            
 *             
 * 
 */

// Declare Library 
#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

//declare all variables
SoftwareSerial mySoftwareSerial(9, 10); // RX, TX.  Reversed from stated in Datasheet
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);

const int trigPin = 11;           //connects to the echo pin on the distance sensor       
const int echoPin = 12;           //connects to the trigger pin on the distance sensor     
float distance = 0;               //stores the distance measured by the distance sensor

//RGB pin Digital connection
int RedPin = 3;
int GreenPin = 5;
int BluePin = 6;


void setup()
{
  //set up serial port for display message
  Serial.begin(9600);
  mySoftwareSerial.begin(9600);
  
  //set the LED pins to output
  pinMode(RedPin,OUTPUT);
  pinMode(GreenPin,OUTPUT);
  pinMode(BluePin,OUTPUT);
  
  pinMode(trigPin, OUTPUT);   //the trigger pin will output pulses of electricity 
  pinMode(echoPin, INPUT);    //the echo pin will measure the duration of pulses coming back from the distance sensor
 
  stopLight();   //set light off as default

  //this will print serial port if the player is playing or fail
  if(myDFPlayer.begin(mySoftwareSerial)){
     Serial.println("OK");
        
    }else{
      Serial.println("Failed");
      } 
}

void loop()
{
  distance = getDistance();   //variable to store the distance measured by the sensor
  if(distance <= 40)
  {     
    play();
    randomColor();       
  }
  else
  { 
    stopLight();
    myDFPlayer.stop();
    
  } 
 
}

//------------------FUNCTIONS-------------------------------

//RETURNS THE DISTANCE MEASURED BY THE HC-SR04 DISTANCE SENSOR
float getDistance()
{
  float echoTime;                   //variable to store the time it takes for a ping to bounce off an object
  float calculatedDistance;         //variable to store the distance calculated from the echo time
  
  //send out an ultrasonic pulse that's 10ms long
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10); 
  digitalWrite(trigPin, LOW);

  echoTime = pulseIn(echoPin, HIGH);      //use the pulsein command to see how long it takes for the
                                          //pulse to bounce back to the sensor

  calculatedDistance = echoTime / 148.0;  //calculate the distance of the object that reflected the pulse (half the bounce time multiplied by the speed of sound)
  
  return calculatedDistance;              //send back the distance that was calculated
}

//This method plays mp3 and set volume
void play()
{
    myDFPlayer.volume(30);  //Set volume value. From 0 to 30
    myDFPlayer.play(1);  //Play the first mp3
}

//This method set random colors on RGB
void randomColor()
{
    analogWrite(RedPin, random(0, 255));
    analogWrite(GreenPin, random(0, 255));
    analogWrite(BluePin, random(0, 255));
    delay(1000);
}

//This method turn off RGB
void stopLight()
{
  //set all three LED pins to 0 or OFF
    analogWrite(RedPin, 0);
    analogWrite(GreenPin, 0);
    analogWrite(BluePin, 0);
}
